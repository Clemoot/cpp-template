rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))

BINDIR=bin
OBJDIR=$(BINDIR)\obj
SRCDIR=src
LIBDIR=$(SRCDIR)/lib
INCLUDEDIR=$(SRCDIR)/include

CXX=mingw32-g++
ifeq ($(MAKECMDGOALS),build_lib)
CXXFLAGS=-W -pedantic -I $(INCLUDEDIR) -DBUILD_DLL
else ($(MAKECMDGOALS),build_debug)
CXXFLAGS=-W -Wall -pedantic -I $(INCLUDEDIR) -g -ggdb
else
CXXFLAGS=-W -pedantic -I $(INCLUDEDIR)
endif
LDFLAGS=-L$(LIBDIR)


SRC=$(call rwildcard,$(SRCDIR),*.cpp)
OBJ=$(notdir $(SRC:.cpp=.o))
OBJ_IN_DIR=$(addprefix $(OBJDIR)/,$(notdir $(SRC:.cpp=.o)))

EXEC=build

all: mrproper $(BINDIR)/$(EXEC)
build_debug: mrproper $(BINDIR)/$(EXEC)
build_release: mrproper $(BINDIR)/$(EXEC)
build_lib: mrproper $(BINDIR)/lib$(EXEC)

$(BINDIR)/lib$(EXEC): $(OBJ_IN_DIR)
	$(CXX) -o $@.dll $^ $(CXXFLAGS) $(LDFLAGS) -shared -Wl,--out-implib,$(BINDIR)/lib$(EXEC).a

$(BINDIR)/$(EXEC): $(OBJ_IN_DIR)
	$(CXX) -o $@ $^ $(CXXFLAGS) $(LDFLAGS)

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(LDFLAGS)

mrproper: clean
	del /s /q "$(BINDIR)\$(EXEC)"
	del /s /q "$(BINDIR)\$(EXEC).exe"
	del /s /q "$(BINDIR)\lib$(EXEC).a"
	del /s /q "$(BINDIR)\lib$(EXEC).lib"
	del /s /q "$(BINDIR)\lib$(EXEC).dll"

clean:
	rmdir /s /q "$(OBJDIR)" & mkdir $(OBJDIR)